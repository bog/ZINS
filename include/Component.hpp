/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef COMPONENT_HPP
#define COMPONENT_HPP
#include <iostream>
#include <ECS.hpp>

namespace compo
{
  /**
   * Signature of the component, in the alphabetical order.
   */
  enum
  {
    SIGNATURE_NONE = 0
    , SIGNATURE_COLOR
    , SIGNATURE_MOVE
    , SIGNATURE_POSITION
    , SIGNATURE_RECT
    , SIGNATURE_SNAKE
    , SIGNATURE_TXT
  };

  /**
   * Aspect of an entity.
   * Contain ONLY public accessible
   * attributes.
   *
   * @author bog
   * @date 2016
   * @version 1
   */
  struct Component
  {
    Component(signature_t _signature);
    signature_t signature = (1<<SIGNATURE_NONE); /**< Signature of the
						    component. */
    bool is_active = false; /** Component's activation state. */

  };
}
#endif
