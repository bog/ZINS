/* Copyright (C) 2016  bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MANAGER_HPP
#define MANAGER_HPP
#include <iostream>
#include <vector>
#include <memory>

#include <Core.hpp>

/* components */
#include <Component.hpp>
#include <ColorComponent.hpp>
#include <MoveComponent.hpp>
#include <PositionComponent.hpp>
#include <RectComponent.hpp>
#include <SnakeComponent.hpp>
#include <TxtComponent.hpp>
#include <SFML/Graphics.hpp>

/* systems */
#include <System.hpp>
#include <MoveSystem.hpp>
#include <KeyboardSystem.hpp>
#include <RenderSystem.hpp>

/**
 * Maximum amount of entities contained in the
 * manager array.
 */
constexpr int MAX_ENTITIES=200;

/**
 * Data structure representing the
 * array of all the entities.
 *
 * @author bog
 * @date April 2016
 * @version 1
 */
struct EntityArray
{
  std::vector<signature_t> signatures;
  std::vector<compo::ColorComponent> colors;
  std::vector<compo::MoveComponent> moves;
  std::vector<compo::PositionComponent> positions;
  std::vector<compo::RectComponent> rects;
  std::vector<compo::SnakeComponent> snakes;
  std::vector<compo::TxtComponent> txts;

  size_t size; /**< Size of ALL the arrays corresponding to the number
		  of entities in the game. */
};

/**
 * The manager is responsible of containing all the entities
 * and to execute all systems.
 * @see System
 *
 * @author bog
 * @date 2016
 * @version 1
 **/
class Manager
{
public:
  explicit Manager(Core* core);
  virtual ~Manager();

  /**
   * Give access to the game window.
   *
   * @return The game window.
   */
  inline sf::RenderWindow* getWindow() const { return m_window.get(); }

  /**
   * Give access to the core class of the game.
   *
   * @return The core class of the game.
   */
  inline Core* getCore() const { return m_core; }

  /**
   * Update the manager.
   *
   */
  void update();

  /**
   * Give access to the array of entities.
   *
   * @return EntityArray The entity array.
   */
  inline EntityArray& getEntities(){ return *m_entities; }

  /**
   * Add an entity in the entity array.
   * Add also deactivated components for this new entity.
   *
   * @return the id of the entity.
   */
  entity_t addEntity();

  /**
   * Signature of all the components of an entity.
   *
   * @param entity The entity containing all the components.
   * @return signature_t The signature of all the component this entity have.
   */
  signature_t getEntitySignature(entity_t entity);

  /**
   * Signature of a component for a entity.
   *
   * @param entity The entity owning the component.
   * @param compo The component we want the signature.
   * @return The signature of the component if it's active, 0 otherwise.
   */
  signature_t getComponentSignature(entity_t entity, compo::Component& compo);

   /**
   * Test if an entity own a component.
   *
   * @param entity The entity to test.
   * @param composign The signature of the component we are looking for.
   * @return True if the entity own the component, false otherwise.
   */
  bool has(entity_t entity, signature_t composign);

  /**
   * Test if an entity has all the requirement to be treated by the
   * system.
   *
   * @param entity The entity to test.
   * @param requirements All the requirement the entity must have.
   * @return True if the entity has ALL the requirements, false otherwise.
   */
  bool hasRequirements(entity_t entity, std::vector<signature_t> requirements);

  /**
   * Test if an entity hasn't any of the requirement to be treated by the
   * system.
   *
   * @param entity The entity to test.
   * @param requirements All the requirement the entity may not have.
   * @return True if the entity has NOT ANY of the requirements, false otherwise.
   */
  bool hasNoneOfRequirements(entity_t entity, std::vector<signature_t> requirements);

  /**
   * Say if a entity exists in the game.
   *
   * @param entity The entity to test.
   * @return True if the entity exists, false otherwise.
   */
  bool exists(entity_t entity);

  /**
   * Execute all the systems on the entity array.
   */
  void executeAllSystems();

  /**
   * Attach a component to an entity.
   *
   * @param entity The entity that will have the component.
   * @param vec The array corresponding to the component.
   * @param component The component to attach to the entity.
   */
  template<typename C>
    void attach(entity_t entity, std::vector<C>& vec, C component);

  /**
   * Detach a component from an entity.
   *
   * @param entity The entity that won't have the component anymore.
   * @param vec The array corresponding to the component.
   * @param component The component to detach from the entity.
   */
  template<typename C>
    void detach(entity_t entity, std::vector<C>& vec);

protected:
  std::unique_ptr<EntityArray> m_entities; /**< Array of all the entities. */
  std::vector< std::unique_ptr<sys::System> > m_systems; /**< Array of all the systems. */

  Core* m_core; /** Game core. */

  std::unique_ptr<sf::RenderWindow> m_window; /**< Window where entities are rendered. */

private:
  Manager( Manager const& manager ) = delete;
  Manager& operator=( Manager const& manager ) = delete;

};

template<typename C>
void Manager::attach(entity_t entity, std::vector<C>& vec, C component)
{
  if( entity >= vec.size() )
    {
      throw std::invalid_argument("Can't attach component to entity "
			      + std::to_string(entity)
			      + " (does not exists).");
    }

  if( vec[entity].is_active )
    {
      throw std::invalid_argument("Can't attach component to entity "
				  + std::to_string(entity)
				  + " (component already attached).");
    }

  vec[entity] = component;
  m_entities->signatures[entity] |= component.signature;
  vec[entity].is_active = true;
}

template<typename C>
void Manager::detach(entity_t entity, std::vector<C>& vec)
{
  if( entity >= vec.size() )
    {
      throw std::invalid_argument("Can't detach component to entity "
			      + std::to_string(entity)
			      + " (component does not exists).");
    }

  if( !vec[entity].is_active )
    {
      throw std::invalid_argument("Can't detach component to entity "
				  + std::to_string(entity)
				  + " (component not attached).");
    }


  if( vec.empty() )
    {
      std::unique_ptr<C> mock = std::make_unique<C>();
      m_entities->signatures[entity] &= ~mock->signature;
    }
  else
    {
      m_entities->signatures[entity] &= ~vec[0].signature;
    }

  vec[entity].is_active = false;
}

#endif
