/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SYSTEM_HPP
#define SYSTEM_HPP
#include <iostream>
#include <vector>
#include <Component.hpp>

class Manager;
class EntityArray;

namespace sys
{
  
  /**
   * Select a subset of the main entity array
   * and make computation on it.
   *
   * @author bog
   * @date 2016
   * @version 1
   */
  class System
  {
  public:
    explicit System(Manager* manager);
    virtual ~System();

    /**
     * Execute the system on a subset of
     * the entities of the manager.
     */
    virtual void execute() = 0;

  protected:
    Manager* m_manager; /**< Manager the system it belongs to and where
			   are all the entities the system will treat. */
    EntityArray& m_entities;
  private:
    System( System const& system ) = delete;
    System& operator=( System const& system ) = delete;
  };
}
#endif
