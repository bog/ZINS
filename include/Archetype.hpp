/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef ARCHETYPE_HPP
#define ARCHETYPE_HPP
#include <iostream>
#include <Manager.hpp>

/**
 * An archetype is a factory that combine components in order to
 * create more complexe entities.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class Archetype
{
 public:
  explicit Archetype(Manager* manager);
  virtual ~Archetype() = 0;

 protected:
  Manager* m_manager;

 private:
  Archetype( Archetype const& archetype ) = delete;
  Archetype& operator=( Archetype const& archetype ) = delete;
};

#endif
