/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef KEYBOARDSYSTEM_HPP
#define KEYBOARDSYSTEM_HPP
#include <iostream>
#include <System.hpp>
#include <SFML/Graphics.hpp>

namespace sys
{
  /**
   * System that listen for keyboard input in a window.
   *
   * @author bog
   * @date 2016
   * @version 1
   */
  class KeyboardSystem : public System
  {
  public:
    explicit KeyboardSystem(Manager* manager, sf::RenderWindow* window);
    virtual ~KeyboardSystem();

    virtual void execute() override;
  protected:
    sf::RenderWindow* m_window;
    sf::Event m_event; /**< Event used for poll window events.  */
    
  private:
    KeyboardSystem( KeyboardSystem const& keyboardsystem ) = delete;
    KeyboardSystem& operator=( KeyboardSystem const& keyboardsystem ) = delete;
  };
}
#endif
