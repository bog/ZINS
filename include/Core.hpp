/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>

class Manager;

/**
 * Starter class of the game where is set all important
 * constant data (game version, game resolution and so one).
 *
 * @author bog
 * @date April 2016
 * @version 1
 */
class Core
{
public:
  explicit Core();

  /**
   * Start the game.
   */
  void start();

  /**
   * Stop the game.
   */
  void stop();

  virtual ~Core();

protected:
  std::unique_ptr<Manager> m_manager; /**< Manager that contain all then entities and systems. */
  bool m_running; /** True when the game is currently running. */

private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;

};

#endif
