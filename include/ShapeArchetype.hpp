/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SHAPEARCHETYPE_HPP
#define SHAPEARCHETYPE_HPP
#include <iostream>
#include <Archetype.hpp>

/**
 * Archetype that produce shapes entities.
 *
 * @author bog
 * @date 2016
 * @version 1
 */
class ShapeArchetype : public Archetype
{
 public:
  explicit ShapeArchetype(Manager* manager);
  virtual ~ShapeArchetype();

  /**
   * Make a square shaped entity.
   * @param x The abscissa of the square.
   * @param y The ordinate of the square.
   * @param z The depth of the square.
   * @param w The width of the square.
   * @param h The height of the square.
   * @return The new square shaped entity.
   */
  entity_t makeSquare(float x, float y, float z, float w, float h);

  /**
   * Make a square shaped entity based on a grid. Each point of a
   * magnetic square correspond to a node of the grid.
   *
   * @see GridConfig.hpp for the grid dimension.
   *
   * @param x The abscissa of the square.
   * @param y The ordinate of the square.
   * @param z The depth of the square.
   * @param w The width of the square.
   * @param h The height of the square.
   * @return The new square shaped entity.
   */
  entity_t makeMagneticSquare(int x, int y, float z, int w, int h);

 protected:

 private:
  ShapeArchetype( ShapeArchetype const& shapearchetype ) = delete;
  ShapeArchetype& operator=( ShapeArchetype const& shapearchetype ) = delete;
};

#endif
