/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef COLORCOMPONENT_HPP
#define COLORCOMPONENT_HPP
#include <iostream>
#include <Component.hpp>

namespace compo
{
  /**
   * Component that give to an entity a colored aspect.
   *
   * @author bog
   * @date 2016
   * @version 1
   */
  struct ColorComponent : public Component
    {
      ColorComponent();
      explicit ColorComponent(unsigned int _r, unsigned int _g, unsigned int _b);
      explicit ColorComponent(unsigned int _r, unsigned int _g, unsigned int _b, unsigned int _a);

      unsigned int r; /**< Red component of the color. */
      unsigned int g; /**< Green component of the color. */
      unsigned int b; /**< Blue component of the color. */
      unsigned int a; /**< Alpha component of the color. */
    };
}
#endif
