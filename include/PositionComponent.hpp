/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef POSITIONCOMPONENT_HPP
#define POSITIONCOMPONENT_HPP
#include <iostream>
#include <Component.hpp>

namespace compo
{
  /**
   * Component describing a 2D position.
   *
   * @note The z coordinate represente the depth. Component with
   * greater depth should be drawn at least.
   *
   * @author bog
   * @date 2016
   * @version 1
   */
  struct PositionComponent : public Component
  {
    PositionComponent();
    explicit PositionComponent(float _x, float _y, float _z);

    float x; /**< X coordinate*/
    float y; /**< Y coordinate*/
    float z; /**< Z coordinate*/
  };
}
#endif
