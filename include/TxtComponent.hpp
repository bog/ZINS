/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef TXTCOMPONENT_HPP
#define TXTCOMPONENT_HPP
#include <iostream>
#include <Component.hpp>
#include <SFML/Graphics.hpp>

namespace compo
{
  /**
   * Component that represent a text area.
   *
   * @author bog
   * @date 2016
   * @version 1
   */
  struct TxtComponent : public Component
  {
    TxtComponent();
    TxtComponent(std::string _text);
    TxtComponent(std::string _text, sf::Color _color);
    TxtComponent(std::string _text, int _size, sf::Color _color);
    TxtComponent(std::string _text
		 , int _size
		 , std::string font_name
		 , sf::Color _color);

    std::string text;
    int size;
    std::string font_name;
    sf::Color color;
  };
}
#endif
