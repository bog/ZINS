/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MOVECOMPONENT_HPP
#define MOVECOMPONENT_HPP
#include <iostream>
#include <Component.hpp>

namespace compo
{
  /**
   * Describe an entity that can move in 2D.
   *
   * @author bog
   * @date 2016
   * @version 1
   */
  struct MoveComponent : public Component
  {
    MoveComponent();
    explicit MoveComponent(float _dx, float _dy);

    float dx; /**< Movement along x axis. */
    float dy; /**< Movement along y axis. */
  };
}
#endif
