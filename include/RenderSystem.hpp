/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef RENDERSYSTEM_HPP
#define RENDERSYSTEM_HPP
#include <iostream>
#include <System.hpp>
#include <SFML/Graphics.hpp>

namespace sys
{
  /**
   * System used for rendering entities.
   *
   * @author bog
   * @date 2016
   * @version 1
   */
  class RenderSystem : public System
  {
  public:
    explicit RenderSystem(Manager* manager, sf::RenderWindow* window);
    virtual ~RenderSystem();

    virtual void execute() override;
  protected:
    sf::RectangleShape m_shape; /**< Shape used to draw things on the screen. */
    sf::RenderWindow* m_window;
    sf::Vector2f m_default_pos;
    sf::Vector2f m_default_size;
    sf::Color m_default_color;

    sf::Font m_font;
    sf::Text m_text;
    sf::Color m_text_color;
    int m_text_size;

    /**
     * Initialize the shape with a default aspect.
     */
    void initShape();

    /**
     * Initialize the text area with a default aspect.
     */
    void initTxt();

  private:
    RenderSystem( RenderSystem const& rendersystem ) = delete;
    RenderSystem& operator=( RenderSystem const& rendersystem ) = delete;
  };
}

#endif
