#include <Manager.hpp>
#include <Core.hpp>
#include <bitset>
#include <ShapeArchetype.hpp>

Manager::Manager(Core* core)
  : m_core(core)
{
  m_entities = std::make_unique<EntityArray>();
  m_entities->size = 0;

  sf::VideoMode mode = sf::VideoMode::getDesktopMode();
  mode.width = 3 * mode.width / 4;
  mode.height = 3 * mode.height / 4;

  m_window = std::make_unique<sf::RenderWindow>( mode
						 , "ZINS - ZINS Is Not Snake"
						 , sf::Style::Titlebar
						 | sf::Style::Close);

  m_window->setFramerateLimit(60.f);

  m_systems.push_back( std::make_unique<sys::MoveSystem>(this) );
  m_systems.push_back( std::make_unique<sys::KeyboardSystem>( this, m_window.get() ) );
  m_systems.push_back( std::make_unique<sys::RenderSystem>( this, m_window.get() ) );

  ShapeArchetype shape_archetype(this);

  // topbar
  {
    entity_t e = shape_archetype.makeMagneticSquare(0
						    , 0
						    , 0.0
						    , 12
						    , 2);
    attach( e
	    , m_entities->colors
	    , compo::ColorComponent(150, 150, 150) );
  }

  // background
  {

    entity_t e = shape_archetype.makeMagneticSquare(0
						    , 2
						    , 0.0
						    , 12
						    , 10);

    attach( e
	    , m_entities->colors
	    , compo::ColorComponent(4, 139, 154) );

  }

  // a snake
  {
    entity_t e = shape_archetype.makeSquare(128
					    , 128
					    , 0.0
					    , 32
					    , 32);
    attach( e
	    , m_entities->colors
	    , compo::ColorComponent(255, 0, 0) );

    attach( e
	    , m_entities->snakes
	    , compo::SnakeComponent() );
  }

}

entity_t Manager::addEntity()
{
  entity_t id = m_entities->size;

  /* add new component here */
  m_entities->signatures.push_back(0);
  m_entities->colors.push_back( compo::ColorComponent() );
  m_entities->moves.push_back( compo::MoveComponent() );
  m_entities->positions.push_back( compo::PositionComponent() );
  m_entities->rects.push_back( compo::RectComponent() );
  m_entities->snakes.push_back( compo::SnakeComponent() );
  m_entities->txts.push_back( compo::TxtComponent() );

  m_entities->size++;

  return id;
}

signature_t Manager::getComponentSignature(entity_t entity
					   , compo::Component& compo)
{
  if(compo.is_active)
    {
      return compo.signature;
    }

  return compo::SIGNATURE_NONE;
}

signature_t Manager::getEntitySignature(entity_t entity)
{
  return m_entities->signatures[entity];
}

bool Manager::has(entity_t entity, signature_t composign)
{
  return getEntitySignature(entity) & (1<<composign);
}

bool Manager::hasRequirements(entity_t entity, std::vector<signature_t> requirements)
{
  return std::all_of(requirements.begin(), requirements.end(), [this, entity](signature_t signature){
      return has(entity, signature);
    });

}

bool Manager::hasNoneOfRequirements(entity_t entity, std::vector<signature_t> requirements)
{
  return std::all_of(requirements.begin(), requirements.end(), [this, entity](signature_t signature){
      return ! has(entity, signature);
    });

}

bool Manager::exists(entity_t entity)
{
  return entity < m_entities->size;
}

void Manager::executeAllSystems()
{
  for(auto& system : m_systems)
    {
      system->execute();
    }
}

void Manager::update()
{
  executeAllSystems();
}

Manager::~Manager()
{

}
