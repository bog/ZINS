#include <RenderSystem.hpp>
#include <Manager.hpp>
#include <GridConfig.hpp>

namespace sys
{
  RenderSystem::RenderSystem(Manager* manager, sf::RenderWindow* window)
    : System(manager)
    , m_window(window)
    , m_default_pos( sf::Vector2f(0, 0) )
    , m_default_size( sf::Vector2f(32, 32) )
    , m_default_color( sf::Color::Black )
  {
    initShape();
    m_font.loadFromFile("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf");
    initTxt();
  }

  void RenderSystem::initShape()
  {
    m_shape.setPosition(m_default_pos);
    m_shape.setSize(m_default_size);
    m_shape.setFillColor(m_default_color);
  }

  void RenderSystem::initTxt()
  {
    m_text_color = sf::Color::Black;
    m_text_size = 12.0;
    m_text = sf::Text("", m_font, m_text_size);
    m_text.setColor(m_text_color);
  }

  /*virtual*/ void RenderSystem::execute()
  {
    // sort entities by z coordinate
    std::vector<entity_t> zorder_entities(m_entities.size);

    {
      int i = 0;
      std::generate(zorder_entities.begin(), zorder_entities.end(), [&i]{return i++;});
    }

    std::sort(zorder_entities.begin()
	      , zorder_entities.end(), [this](entity_t const e1, entity_t const e2){

	float z1 = 0.0;
	float z2 = 0.0;

	if( m_manager->has(e1, compo::SIGNATURE_POSITION) )
	  {
	    z1 = m_entities.positions[e1].z;
	  }

	if( m_manager->has(e2, compo::SIGNATURE_POSITION) )
	  {
	    z2 = m_entities.positions[e2].z;
	  }

	return z1 < z2;
      });

    m_window->clear(sf::Color::White);

    // display sorted entities
    for(entity_t& e : zorder_entities)
      {
	if( m_manager->hasNoneOfRequirements(e, {
	      compo::SIGNATURE_POSITION
		, compo::SIGNATURE_RECT}) )
	  {
	    continue;
	  }

	initShape();

	if( m_manager->has(e, compo::SIGNATURE_POSITION) )
	  {
	    m_shape.setPosition(sf::Vector2f(m_entities.positions[e].x
					     , m_entities.positions[e].y));
	    m_text.setPosition( m_shape.getPosition() );
	  }

	if( m_manager->has(e, compo::SIGNATURE_RECT) )
	  {
	    m_shape.setSize(sf::Vector2f(m_entities.rects[e].w
					 , m_entities.rects[e].h));

	  }

	if( m_manager->has(e, compo::SIGNATURE_COLOR) )
	  {
	    m_shape.setFillColor(sf::Color(m_entities.colors[e].r
					   , m_entities.colors[e].g
					   , m_entities.colors[e].b
					   , m_entities.colors[e].a));
	  }

	m_window->draw(m_shape);

	if( m_manager->has(e, compo::SIGNATURE_TXT) )
	  {
	    m_text.setString(m_entities.txts[e].text);
	    m_text.setCharacterSize(m_entities.txts[e].size);
	    m_text.setColor(m_entities.txts[e].color);
	    m_window->draw(m_text);
	  }

      }

    m_window->display();
  }

  RenderSystem::~RenderSystem()
  {

  }
}
