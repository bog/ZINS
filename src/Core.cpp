#include <Core.hpp>
#include <Manager.hpp>

Core::Core()
  : m_running(true)
{
  m_manager = std::make_unique<Manager>(this);
}

void Core::start()
{
  while(m_running)
    {
      m_manager->update();
    }
}

void Core::stop()
{
  m_running = false;
}

Core::~Core()
{

}
