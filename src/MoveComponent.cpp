#include <MoveComponent.hpp>

namespace compo
{
  MoveComponent::MoveComponent()
    : MoveComponent(0.f, 0.f)
  {

  }

  /*explicit*/ MoveComponent::MoveComponent(float _dx, float _dy)
    : Component(SIGNATURE_MOVE)
    , dx(_dx)
    , dy(_dy)
  {

  }
}
