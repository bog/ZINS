#include <System.hpp>
#include <Manager.hpp>


namespace sys
{
  System::System(Manager* manager)
    : m_manager(manager)
    , m_entities( m_manager->getEntities() )
  {
  
  }
  
  System::~System()
  {
  
  }
}
