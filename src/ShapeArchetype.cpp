#include <ShapeArchetype.hpp>
#include <GridConfig.hpp>

ShapeArchetype::ShapeArchetype(Manager* manager)
  : Archetype(manager)
{

}

entity_t ShapeArchetype::makeSquare(float x, float y, float z, float w, float h)
{
  entity_t e = m_manager->addEntity();

  m_manager->attach(e
		    , m_manager->getEntities().positions
		    , compo::PositionComponent(x, y, z) );

  m_manager->attach(e
		    , m_manager->getEntities().rects
		    , compo::RectComponent(w, h) );
  return e;
}

entity_t ShapeArchetype::makeMagneticSquare(int x, int y, float z, int w, int h)
{
  sf::Vector2u win_size = m_manager->getWindow()->getSize();

  return makeSquare( (x * win_size.x)/GRID_WIDTH
		     , (y * win_size.y)/GRID_HEIGHT
		     , z
		     , (w * win_size.x)/GRID_WIDTH
		     , (h * win_size.y)/GRID_HEIGHT );
}

ShapeArchetype::~ShapeArchetype()
{

}
