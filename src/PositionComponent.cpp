#include <PositionComponent.hpp>

namespace compo
{
  PositionComponent::PositionComponent()
    : PositionComponent(0.f, 0.f, 0.f)
  {
  }

  /*explicit*/ PositionComponent::PositionComponent(float _x, float _y, float _z)
    : Component(SIGNATURE_POSITION)
    , x(_x)
    , y(_y)
    , z(_z)
  {
  }
}
