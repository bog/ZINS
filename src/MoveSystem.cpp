#include <MoveSystem.hpp>
#include <Manager.hpp>

namespace sys
{
  /*explicit*/ MoveSystem::MoveSystem(Manager* manager)
    : System(manager)
  {

  }

  /*virtual*/ void MoveSystem::execute()
  {
    for(size_t e=0; e<m_entities.size; e++)
      {
	if( !m_manager->hasRequirements(e, {compo::SIGNATURE_POSITION
		, compo::SIGNATURE_MOVE}) )
	  {
	    continue;
	  }

	m_entities.positions[e].x += m_entities.moves[e].dx;
	m_entities.positions[e].y += m_entities.moves[e].dy;
      }
  }

  MoveSystem::~MoveSystem()
  {

  }
}
