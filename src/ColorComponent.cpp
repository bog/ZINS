#include <ColorComponent.hpp>

namespace compo
{
  ColorComponent::ColorComponent()
    : ColorComponent(0, 0, 0)
  {

  }

  /*explicit*/ ColorComponent::ColorComponent(unsigned int _r, unsigned int _g, unsigned int _b)
    : ColorComponent(_r, _g, _b, 255)
  {
  }

  /*explicit*/ ColorComponent::ColorComponent(unsigned int _r, unsigned int _g, unsigned int _b, unsigned int _a)
    : Component(SIGNATURE_COLOR)
    , r(_r)
    , g(_g)
    , b(_b)
    , a(_a)
  {
  }
}
