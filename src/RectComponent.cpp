#include <RectComponent.hpp>

namespace compo
{

  RectComponent::RectComponent()
    : RectComponent(0.f, 0.f)
  {
  }

  /*explicit*/ RectComponent::RectComponent(float _w, float _h)
    : Component(SIGNATURE_RECT)
    , w(_w)
    , h(_h)
  {
  }

}
