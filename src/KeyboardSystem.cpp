#include <KeyboardSystem.hpp>
#include <Manager.hpp>

namespace sys
{
  KeyboardSystem::KeyboardSystem(Manager* manager, sf::RenderWindow* window)
    : System(manager)
    , m_window(window)
  {

  }

  /*virtual*/ void KeyboardSystem::execute()
  {
    while( m_window->pollEvent(m_event) )
      {
	// close window
	if(m_event.type == sf::Event::Closed)
	  {
	    m_manager->getCore()->stop();
	  }

	if(m_event.type == sf::Event::KeyPressed)
	  {
	    switch(m_event.key.code)
	      {
	      case sf::Keyboard::Escape:
		m_manager->getCore()->stop();
		break;
	      default:break;
	      }
	  }
      }
  }

  KeyboardSystem::~KeyboardSystem()
  {

  }
}
