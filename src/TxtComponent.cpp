/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <TxtComponent.hpp>

namespace compo
{
  TxtComponent::TxtComponent()
    : TxtComponent("")
  {
    
  }

  TxtComponent::TxtComponent(std::string _text)
    : TxtComponent(_text, sf::Color::Black)
  {
    
  }

  TxtComponent::TxtComponent(std::string _text, sf::Color _color)
    : TxtComponent(_text
		   , 12
		   , _color)
  {
    
  }

  TxtComponent::TxtComponent(std::string _text
			     , int _size
			     , sf::Color _color)
    : TxtComponent(_text, _size, "FreeSans.ttf", _color)
  {
    
  }
  
  TxtComponent::TxtComponent(std::string _text
			     , int _size
			     , std::string _font_name
			     , sf::Color _color)
    : Component(SIGNATURE_TXT)
    , text(_text)
    , size(_size)
    , font_name(_font_name)
    , color(_color)
  {
    
  }
			     
		 
}
